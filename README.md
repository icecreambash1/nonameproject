## Настройка перед применением

Откройте файл .env и заполните значения
<p>NOCAPTCHA_SECRET=GOOGLE_SECRET <- Секретный ключ капчи </p>
<p> NOCAPTCHA_SITEKEY=GOOGLE_SITEKEY <- Ключ капчи </p>
<p> api_token_crypto=CRYPTO_TOKEN <- Токен от coinmarketcap.com (https://coinmarketcap.com/api/) </p>
<p> api_token_cryptocompare=CRYPTO_COMPARE_TOKEN <- Токен от cryptocompare https://min-api.cryptocompare.com/ </p>
<p>
Откройте конфиг php(php.ini) и укажите ключ к "curl.cainfo" путь до сертификата (cacert.pem)
Пример: curl.cainfo = "D:\php\nonameproject\cacert.pem"

## Требования

PHP >= 8.1 <br>
DB >= MySQL or MariaDB

## Установка 
<p>Создайте базу данных laravel и заполните .env
</p>
<p>DB_DATABASE={имя базы данных}</p>
<p>DB_USERNAME={имя пользователя бд}</p>
<p>DB_PASSWORD={Пароль к бд}</p>
<br>
<p>php artisan migrate</p>
<p>php artisan command:getcrypto</p>
<p>php artisan command:getcryptoprices</p>
<p>composer install</p>
<p>npm run dev</p>
<p>php artisan serve</p>

## Роуты
<p>/auth/register   <span style="color:green">GET</span> </p>
<p>/auth/register   <span style="color:red">POST</span> </p>
<p>/auth/login   <span style="color:green">GET</span> </p>
<p>/auth/login   <span style="color:red">POST</span> </p>
<br>
<p>/lk/   <span style="color:green">GET</span> </p>
<p>/lk/   <span style="color:red">POST</span> </p>
<p>/lk/history   <span style="color:green">GET</span> </p>
<p>/lk/balance   <span style="color:green">GET</span> </p>
<p>/lk/balance   <span style="color:red">POST</span> </p>
<p>/lk/crypto/{id}/   <span style="color:green">GET</span> </p>
<p>/lk/crypto/{id}/   <span style="color:red">POST</span> </p>
<br>
<p>/user/logout/   <span style="color:green">GET</span> </p>



