<?php

namespace App\Console\Commands;

use App\Http\Services\Postman\Api\GetCryptoPrices;
use Illuminate\Console\Command;

class GetPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getcryptoprices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $class = new GetCryptoPrices();
        $class->getPrices();
    }
}
