<?php

namespace App\Console\Commands;

use App\Http\Services\Postman\Api\Crypto;
use Illuminate\Console\Command;

class getCrypto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getcrypto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */



    public function handle()
    {
        $class = new Crypto();
        $class->getCrypto();
    }
}
