<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Login\StoreLoginRequest;
use App\Http\Services\Login\LoginService;

class LoginController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('login.main');
    }

    /**
     * @param StoreLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreLoginRequest $request): \Illuminate\Http\RedirectResponse
    {

        $request->validated();

        $data = $request->only(['login', 'password']);

        $data = LoginService::login($data);

        return $data;
    }
}
