<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Helper\AuthHelper\AuthHelper;
use App\Http\Requests\Register\StoreRegisterRequest;
use App\Http\Services\Register\RegisterService;
use App\Http\Services\Wallet\WalletService;

class RegisterController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('register.main');
    }

    /**
     * @param StoreRegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRegisterRequest $request): \Illuminate\Http\RedirectResponse
    {

        $request->validated();

        $data = $request->only(['login', 'password']);
        $user = RegisterService::register($data);

        WalletService::createWalletUser($user);
        return redirect()->route('login');
    }
}
