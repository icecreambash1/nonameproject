<?php

namespace App\Http\Controllers\LK\Crypto;

use App\Http\Controllers\Controller;
use App\Http\Helper\AuthHelper\AuthHelper;
use App\Http\Helper\CryptoHelper\CryptoHelper;
use App\Http\Requests\Crypto\CryptoOrderRequest;
use App\Http\Requests\Crypto\ValidateIdCryptoRequest;
use App\Http\Services\Crypto\CryptoService;
use Illuminate\Http\Request;

class CryptoController extends Controller
{
    /**
     * @param ValidateIdCryptoRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(ValidateIdCryptoRequest $request, int $id) : \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View {
        $request->validated();
        return view('crypto.main',['name'=>CryptoHelper::getImageCrypto($id),'id'=>$id]);
    }

    /**
     * @param CryptoOrderRequest $request
     * @param integer $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CryptoOrderRequest $request, int $id) : \Illuminate\Http\RedirectResponse {
        $request->validated();

        $data = $request->only(
            [
                'count'
            ]
        );

        return CryptoService::order(AuthHelper::user(),$id,$data);

    }
}
