<?php

namespace App\Http\Controllers\LK\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Helper\AuthHelper\AuthHelper;
use App\Http\Services\Cash\CashService;
use App\Http\Services\History\HistoryService;
use App\Models\Cryptocurrency;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BalanceController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $user = AuthHelper::user()->wallet()->get();

        $response = [];

        foreach ($user as $us) {

            array_push($response, [
                'data' => $us,
                'crypto' => Cryptocurrency::find($us->cryptocurrency_id)
            ]);
        }
        return view('balance.main', ['wallet' => $response]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(): \Illuminate\Http\RedirectResponse
    {
        $user = AuthHelper::pureUser()->first()
            ->history()
            ->where('type', '=', 'bonus')
            ->latest('created_at')
            ->first();

        if (!$user) {
            CashService::bonusEveryday(AuthHelper::user());
            HistoryService::bonusEveryHour(AuthHelper::user(), 25000);
            return back();
        } else {
            $rule = Carbon::parse($user->created_at)->addHour();
        }

        if (Carbon::now() > $rule) {
            CashService::bonusEveryday(AuthHelper::user());
            HistoryService::bonusEveryHour(AuthHelper::user(), 25000);
            return back();
        } else {
            return back()->withErrors(['not_time' => 'Попробуйте позже']);
        }
    }

}

