<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Controllers\LK\Dashboard;

use App\Http\Services\Dashboard\DashboardService;
use App\Models\Cryptocurrency;
use Illuminate\Http\Request;

class DashboardController
{
    /**
     * @param Request $request
     * @param string field
     * @param string direction
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $requestData = $request->all();
        if (array_key_exists('field', $requestData) && array_key_exists('direction', $requestData))
            return view('dashboard.main',
                ['cryptocurrencies' => DashboardService::getCryptocurrenciesSorted($requestData['field'], $requestData['direction'])]);
        else
            return view('dashboard.main',
                ['cryptocurrencies' => DashboardService::getCryptocurrenciesSorted()]);
    }

    public function searchCrypto(Request $request)
    {
        $requestData = $request->all();
        if($requestData['request']==null)
            return redirect('/lk');
        return view('dashboard.main',
            ['cryptocurrencies' => DashboardService::findByShortName($requestData['request'])]);
    }
}
