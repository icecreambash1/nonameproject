<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Controllers\LK\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Services\History\HistoryService;
use App\Models\Session;

class HistoryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('history.main')->with(['history' => HistoryService::getPaginationHistory()]);
    }
}
