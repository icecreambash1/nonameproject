<?php

namespace App\Http\Controllers;

use App\Http\Services\Dashboard\DashboardService;

class LandingController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('landing.main');
    }

    public function exchange(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        return view('landing.exchange',['cryptocurrencies' => DashboardService::getCryptocurrenciesSorted()]);
    }

}
