<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Helper\AuthHelper;

use App\Models\Session;

class AuthHelper implements AuthHelperInterface
{
    public static function check(): bool
    {
        $token = session()->get('session_key');
        $session = Session::where('token','=',$token);
        if($session->first()->user()->first()->login) {
            return true;
        }
        return false;
    }
    public static function user() {
        $token = session()->get('session_key');
        $session = Session::where('token','=',$token);
        $user = $session->first()->user()->first();
        return $user;
    }
    public static function pureUser() {
        $token = session()->get('session_key');
        $session = Session::where('token','=',$token);
        $user = $session->first()->user();
        return $user;
    }
}
