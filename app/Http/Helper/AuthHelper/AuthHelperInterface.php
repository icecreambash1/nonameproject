<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Helper\AuthHelper;

interface AuthHelperInterface
{
    public static function check(): bool;
    public static function user();
    public static function pureUser();
}
