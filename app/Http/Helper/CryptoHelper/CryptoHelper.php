<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Helper\CryptoHelper;

use App\Http\Services\Crypto\CryptoServiceInterface;
use App\Models\Cryptocurrency;

class CryptoHelper implements CryptoHelperInterface
{
    public static function getImageCrypto($id) {
        $crypto = Cryptocurrency::find($id);
        return $crypto->icon_img_path;
    }
    public static function getCryptoModel($id) {
        $crypto = Cryptocurrency::find($id);
        return $crypto;
    }
}
