<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Helper\CryptoHelper;

interface CryptoHelperInterface
{
    public static function getImageCrypto($id);
    public static function getCryptoModel($id);
}
