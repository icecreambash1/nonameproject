<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Helper\PaginatorHelper;

interface PaginatorHelperInterface
{
    public static function getLastPage($to,$fo);
    public static function getFirstPage($to);
}
