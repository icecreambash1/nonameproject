<?php

namespace App\Http\Requests\Crypto;

use Illuminate\Foundation\Http\FormRequest;

class CryptoOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge(['id' => $this->route('id')]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'=>'exists:cryptocurrencies,id|integer',
            'count'=>'required|numeric|min:0',
        ];
    }

    public function messages()
    {
        return [
            'count.required'=>'Обязательное поле',
            'count.numeric'=>'Поле должно содержать только цифры',
            'count.min'=>'Поле не должно быть меньше 0'
        ];
    }
}
