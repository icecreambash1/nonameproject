<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Requests\Login;

use Illuminate\Foundation\Http\FormRequest;

class StoreLoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'login'=>'required|exists:users,login|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'password'=>'required',
        ];
    }
    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        return back()->withErrors($validator)->withInput();
    }

    public function messages()
    {
        return [
            'login.required'=>'Логин обязателен',
            'login.exists'=>'Логина не существует',
            'password.required'=>'Пароль обязателен',
            'login.regex'=>'Логин содержит не поддерживаемые символы [a-Z]'
        ];
    }
}
