<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'login'=>'required|unique:users,login|regex:/(^([a-z]+)(\d+)?$)/u',
            'password'=>'required|min:8',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    protected function failedValidation(Validator|\Illuminate\Contracts\Validation\Validator $validator)
    {
        return back()->withErrors($validator)->withInput();
    }

    public function messages()
    {
        return [
            'login.required'=>'Логин обязателен',
            'login.unique'=>'Логин занят',
            'password.required'=>'Пароль обязателен',
            'password.min'=>'Пароль должен состоять минимум из 8 символов',
            'g-recaptcha-response.required'=>'Капча обязательна',
            'g-recaptcha-response.captcha'=>'Капча не пройдена',
            'login.regex'=>'Логин содержит не поддерживаемые символы [a-Z]'
        ];
    }
}
