<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Balance;

class BalanceService implements BalanceServiceInterface
{
    public static function purchase($user, $money) {
        $user->cash -= $money;
        $user->save();
    }
}
