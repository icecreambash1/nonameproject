<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Balance;

interface BalanceServiceInterface
{
    public static function purchase($user, $money);
}
