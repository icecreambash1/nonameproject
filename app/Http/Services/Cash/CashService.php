<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Cash;

use App\Http\Services\History\HistoryService;

class CashService implements CashServiceInterface
{
    public static function bonusRegister($user) {
        $user->cash += 50000;
        $user->save();

        HistoryService::refillCash($user, 50000);
    }

    public static function bonusEveryday($user) {
        $user->cash += 25000;
        $user->save();
    }
}
