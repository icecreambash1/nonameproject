<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Cash;

interface CashServiceInterface
{
    public static function bonusRegister($user);
    public static function bonusEveryday($user);
}
