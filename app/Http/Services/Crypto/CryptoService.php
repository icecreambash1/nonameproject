<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Crypto;

use App\Http\Services\Balance\BalanceService;
use App\Http\Services\History\HistoryService;
use App\Models\Cryptocurrency;

class CryptoService implements CryptoServiceInterface
{
    public static function order($user, $id, $count)
    {
        $moneyuser = $user->cash;
        $token = Cryptocurrency::find($id);
        $coursetoken = $token->price * 0.95;

        if ($moneyuser < $coursetoken * (float)$count['count']) {
            return back()->withErrors(['count' => 'У вас недостаточно денег']);
        }

        $wallet = $user->wallet()->where('cryptocurrency_id', '=', $id)->first();
        $wallet->update(
            [
                'count' =>  $wallet->count += $count['count']
            ]
        );
        BalanceService::purchase($user, (float)$coursetoken * (float)$count['count']);
        HistoryService::tradeCrypto($user, (int)((float)$coursetoken * (float)$count['count']));
        session()->flash('alert_order', 'true');
        return back();

    }
}
