<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Crypto;

interface CryptoServiceInterface
{
    public static function order($user, $id, $count);
}
