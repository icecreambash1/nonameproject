<?php

namespace App\Http\Services\Dashboard;

use App\Models\Cryptocurrency;
use Illuminate\Support\Facades\DB;

class DashboardService implements DashboardServiceInterface
{
    public static function getCryptocurrenciesSorted(string $field = 'price', $direction = 'desc') : \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return DB::table('cryptocurrencies')->orderBy($field, $direction)->paginate(10);
    }

    public static function findByShortName(string $name) : \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return DB::table('cryptocurrencies')->where('short_name', 'LIKE', '%'. $name.'%')->paginate(10);
    }
    public static function getAll() {
        return Cryptocurrency::all();
    }
}
