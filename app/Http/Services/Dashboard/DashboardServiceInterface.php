<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Dashboard;

interface DashboardServiceInterface
{
    public static function getCryptocurrenciesSorted(string $field, string $direction): \Illuminate\Contracts\Pagination\LengthAwarePaginator;

    public static function findByShortName(string $name): \Illuminate\Contracts\Pagination\LengthAwarePaginator;

    public static function getAll();
}
