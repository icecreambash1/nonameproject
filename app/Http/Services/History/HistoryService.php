<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\History;

use App\Models\Session;
use Illuminate\Support\Carbon;

class HistoryService implements HistoryServiceInterface
{
    public static function refillCash($user, $money) {
        $node = $user->history();
        $node->create(
            [
                'type'=>'refill',
                'value'=>$money,
                'from_title'=>'Администратор',
                'created_at'=>Carbon::now(),
            ]
        );
    }

    public static function getPaginationHistory() {
        $token = session()->get('session_key');
        $session = Session::where('token','=',$token)->first();
        $user = $session->user()->first();
        $history = $user->history()->paginate(10);
        return $history;
    }

    public static function tradeCrypto($user,$money) {
        $node = $user->history();
        $node->create(
            [
                'type'=>'buy',
                'value'=>$money,
                'from_title'=>'Система',
                'created_at'=>Carbon::now(),
            ]
        );
    }

    public static function bonusEveryHour($user,$money) {
        $node = $user->history();
        $node->create(
            [
                'type'=>'bonus',
                'value'=>$money,
                'from_title'=>'Администратор',
                'created_at'=>Carbon::now(),
            ]
        );
    }
}
