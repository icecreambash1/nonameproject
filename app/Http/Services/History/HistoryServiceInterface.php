<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\History;

interface HistoryServiceInterface
{
    public static function refillCash($user, $money);
    public static function getPaginationHistory();
    public static function tradeCrypto($user,$money);
    public static function bonusEveryHour($user,$money);
}
