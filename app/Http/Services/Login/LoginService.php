<?php

namespace App\Http\Services\Login;

use App\Models\User;
use http\Client\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class LoginService implements LoginServiceInterface
{
    public static function login($bottle)
    {
        $token = sha1(time());
        $user = User::where('login', '=', $bottle['login'])->first();
        if (Hash::check($bottle['password'], $user->password)) {
            $session = User::find($user->id);
            $user = $session->session()->create(
                [
                    'token'=> $token
                ]
            );
            session()->put('session_key',$token);
            return \redirect()->route('lk');
        }
        return Redirect::back()->withErrors(['password'=>'Пароль не правильный']);
    }
}
