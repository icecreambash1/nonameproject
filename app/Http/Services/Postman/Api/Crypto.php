<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Postman\Api;

use App\Models\Cryptocurrency;
use GuzzleHttp\Client;

class Crypto
{

    private string $token;
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri'=>'https://pro-api.coinmarketcap.com'
        ]);
        $this->token = env('api_token_crypto');
    }

    public function getCrypto() {
        $response = $this->client->request('GET','/v1/cryptocurrency/map?start=1&limit=10&sort=cmc_rank', [
            'headers'=> [
                'X-CMC_PRO_API_KEY'=>$this->token
            ]
        ]);
        $data = json_decode($response->getBody(),true);

        foreach ($data['data'] as $item) {
            $toSave = [];
            $toSave['name'] = $item['name'];
            $toSave['short_name'] = $item['symbol'];
            Cryptocurrency::updateOrCreate([
                'name' => $item['name'],
                'short_name' => $item['symbol'],
                'price' => rand(200,5000),
                'icon_img_path'=>$item['symbol'].'.png',
            ]);
        }

    }


}
