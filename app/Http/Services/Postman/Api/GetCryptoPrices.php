<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Postman\Api;

use App\Models\Cryptocurrency;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class GetCryptoPrices
{
    private string $token;
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://min-api.cryptocompare.com'
        ]);
        $this->token = env('api_token_cryptocompare');
    }

    public function getPrices(array $cryptoNamesArray = [])
    {
        $result = [];
        $cryptoNames = '';
        if($cryptoNamesArray == [])
        {
            $oldCrypto = Cryptocurrency::all()->toArray();

            //Получаем названия криптовалют
            $cryptoNamesArray = [];
            foreach ($oldCrypto as $item) {
                array_push($cryptoNamesArray, $item['short_name']);
            }


        }
        //Разбиваем на части, чтобы пройти под ограничение длинны параметра запроса
        $namesBatches = array_chunk($cryptoNamesArray, 10);

        //Отправляем названия валют, сохраняем и получаем цены
        foreach ($namesBatches as $batch){
            array_push($result, $this->getSaveBatch($batch));
        }

        return $result;
    }

    private function getSaveBatch(array $namesArray)
    {
        $cryptoNames = implode(',', $namesArray);

        $response = $this->client->request('GET', "/data/pricemulti?fsyms=$cryptoNames&tsyms=USD,EUR&api_key={$this->token}");
        $data = json_decode($response->getBody(), true);

        $iter = 0;
        $updatedCryproPrices =[];
        foreach ($data as $item) {
            $cry = $namesArray[$iter];
            Cryptocurrency::where('short_name', $cry)
                ->update(['price' => round($item['USD'],2)]);

            $updatedCryproPrices[$cry] = $item['USD'];
            $iter++;
        }

        return $updatedCryproPrices;
    }

    private function save($data)
    {

    }
}
