<?php

namespace App\Http\Services\Register;

use App\Http\Services\Cash\CashService;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterService implements RegisterServiceInterface
{
    public static function register($bottle) {
        $user = User::create([
            'login'=>$bottle['login'],
            'password'=>Hash::make($bottle['password']),
            'cash'=>0,
        ]);
        CashService::bonusRegister($user);
        return $user;
    }
}
