<?php

namespace App\Http\Services\Register;

interface RegisterServiceInterface
{
    public static function register($bottle);
}
