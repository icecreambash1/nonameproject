<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Wallet;

use App\Models\Cryptocurrency;

class WalletService implements WalletServiceInterface
{
    public static function createWalletUser($user) {
        $crypto = Cryptocurrency::all();
        foreach ($crypto as $cry) {
            $wallet = $user->wallet()->create(
                [
                    'cryptocurrency_id'=>$cry->id,
                    'count'=>0
                ]
            );
        }

    }
}
