<?php
/*
 * Copyright (c) Kostylev Artem <kostylev.artjom@yandex.ru> 2022.
 */

namespace App\Http\Services\Wallet;

interface WalletServiceInterface
{
    public static function createWalletUser($user);
}
