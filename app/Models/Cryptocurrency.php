<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cryptocurrency extends Model
{
    public $timestamps = false;
    protected $table = "cryptocurrencies";

    protected $fillable = [
        'name',
        'short_name',
        'price',
        'icon_img_path'
    ];
}
