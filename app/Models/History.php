<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{

    protected $fillable = [
        'user_id',
        'type',
        'value',
        'from_title',
        'from',
    ];
}
