<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'login',
        'password',
        'cash',
    ];

    protected $hidden = [
        'password'
    ];

    public function session() {
        return $this->hasMany(Session::class);
    }

    public function history() {
        return $this->hasMany(History::class);
    }
    public function wallet() {
        return $this->hasMany(Wallet::class);
    }
}
