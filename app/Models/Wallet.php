<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{

    public $timestamps = false;

    protected $hidden = [
        'cryptocurrency_id',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'count',
        'cryptocurrency_id',
    ];
}
