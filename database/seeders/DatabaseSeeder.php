<?php

namespace Database\Seeders;

use Database\Factories\HistoryFactory;
use Faker\Provider\ru_RU\Company;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $history = new DatabaseSeeder();
        for ($i=0;$i=20;) {
            DB::table('histories')->insert([
                'user_id'=>18,
                'type'=>'refill',
                'value'=>rand(200,500),
                'from_title'=>Company::asciify()
            ]);
        }
    }
}
