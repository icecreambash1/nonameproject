<!doctype html>
<html lang="{{app()->getLocale()}}">
@include('layoutAuth',['title'=>'Баланс'])
<body>
@include('navbar')
<div class="container-sm" style="text-align: center">
    <h1 class="text-center text-primary">Баланс</h1>
    <h2 class="text-center">{{\App\Http\Helper\AuthHelper\AuthHelper::user()->cash}} $</h2>
    <div class="container">
        <div class="row justify-content-center" style="margin-top:50px;">
            <form action="{{route('balance')}}" method="post">
                @csrf
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example13">
                        Пополнить баланс на 25000$
                        <span class="text-muted">(можно раз в 1 час)</span>
                        @if($errors->has('not_time'))
                            <p class="text-danger">{{$errors->first('not_time')}}</p>
                        @endif
                    </label>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Пополнить</button>
            </form>
        </div>
        <h1 class="text-center">Кошелек</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Название</th>
                <th scope="col">Количество</th>
                <th scope="col">Покупка</th>
            </tr>
            </thead>
            @foreach($wallet as $wal)
                {{--            @dd($wal['data']->id)--}}
                <tr>
                    <th scrope="row"><img width="30" height="30" src="/res/coins/{{$wal['crypto']->icon_img_path}}">
                    </th>
                    <td>{{$wal['crypto']->short_name}}</td>
                    <td>{{$wal['data']->count}}</td>
                    <td>
                        <a href="{{route('crypto',$wal['crypto']->id)}}">
                            <button type="button" class="btn btn-success">Купить</button>
                        </a>
                    </td>
                </tr>
            @endforeach
            <tbody>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
