<!doctype html>
<html lang={{app()->getLocale()}}>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
@include('layoutAuth',['title'=>'Покупка'])
<body>
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path
            d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
    </symbol>
    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
        <path
            d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
    </symbol>
    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path
            d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
</svg>
@include('navbar')
<div class="container-sm">
    @if(session()->has('alert_order'))
        <div class="alert alert-success d-flex align-items-center" role="alert" style="margin-top: 20px">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                <use xlink:href="#check-circle-fill"/>
            </svg>
            <div>
                Покупка прошла!
            </div>
        </div>
    @endif
    <div class="text-center" style="margin-top: 30px">
        <img width="150" height="150" src="/res/coins/{{$name}}">
        <h1>{{\App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->name}}</h1>
        <h4 class="text-muted">{{App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->short_name}}</h4>
        <div class="chart-container">
            <canvas id="line-chart" width="800" height="450"></canvas>
        </div>
        <p class="">Курс <span
                class="text-danger">~{{\App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->price*0.95}}</span>
            $</p>
        <p class="">Ваш баланс <span
                class="text-success">{{\App\Http\Helper\AuthHelper\AuthHelper::user()->cash}}</span> $</p>
        <div class="row justify-content-center" style="margin-top:50px;">
            <form action="{{route('crypto',$id)}}" method="post">
                <!-- Email input -->
                @csrf
                <div class="form-outline mb-4">
                    <label class="form-label" for="form1Example13">
                        Кол-во {{\App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->name}}
                        <input type="number" id="moneytransfer" class="form-control" name="count"
                               maxlength="
                           {{round(\App\Http\Helper\AuthHelper\AuthHelper::user()->cash / \App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->price,2)}}
                                   "
                               placeholder="
                           {{\App\Http\Helper\AuthHelper\AuthHelper::user()->cash / \App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->price}}
                           "
                               step="0.00"
                        >
                        @error('count')
                        <p class="text-danger">{{$message}}</p>
                        @enderror
                    </label>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Купить</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>

<script>
    fetch('https://min-api.cryptocompare.com/data/v2/histoday?fsym=' +
        '{{\App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->short_name}}' +
        '&tsym=USD&limit=30&e=CCCAGG&api_key=e15777c9032668a0ef86fec26f2f1b7abf0f106862246d008957ef094f593fd0')
        .then(res => res.json())
        .then(r => {
            var dates = [];
            var prices = [];
            r.Data.Data.forEach(item => {
                var date = new Date(item.time * 1000);
                var formatedDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                dates.push(formatedDate);
                prices.push(item.open);
            })
            new Chart(document.getElementById("line-chart"), {
                type: 'line',
                data: {
                    labels: dates,
                    datasets: [{
                        data: prices,
                        label: "{{\App\Http\Helper\CryptoHelper\CryptoHelper::getCryptoModel($id)->name}}",
                        borderColor: "#3e95cd",
                        fill: false
                    }
                    ]
                },
                options: {
                    title: {
                        display: true
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }
            });
        });
</script>
