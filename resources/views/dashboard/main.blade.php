<!doctype html>
<html lang="en">
<head>
    @include('layoutAuth',['title'=>'Дашборд'])
</head>
<body>
@include('navbar')
<div class="container-sm" style="text-align: center">
    <div class="row mt-5">
        <div class="col-md-5 mx-auto">
            <form action="{{route('searchCrypto')}}" method="post">
                @csrf
                <div class="input-group">
                    <input class="form-control border-end-0 border" type="search" id="example-search-input"
                           name="request">
                    <span class="input-group-append">
                    <button class="btn btn-outline-secondary bg-white border-start-0 border-bottom-0 border ms-n5"
                            type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                </div>
            </form>
        </div>
    </div>

    <div class="dropdown" style="margin-bottom:25px; margin-top:25px;">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1"
                data-bs-toggle="dropdown" aria-expanded="false">
            Сортировка
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="{{route('lk', ['field' => 'price', 'direction'=>'asc'])}}">По
                    возрастанию цены</a></li>
            <li><a class="dropdown-item" href="{{route('lk', ['field' => 'price', 'direction'=>'desc'])}}">По убыванию
                    цены</a></li>
            <li><a class="dropdown-item" href="{{route('lk', ['field' => 'short_name', 'direction'=>'asc'])}}">В
                    алфавитном порядке</a></li>
            <li><a class="dropdown-item" href="{{route('lk', ['field' => 'short_name', 'direction'=>'desc'])}}">В
                    обратном алфавитном порядке</a></li>
        </ul>
    </div>

    @if($cryptocurrencies->first())
        <table class="table">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Название</th>
                <th scope="col">Курс ~$</th>
                <th scope="col">Биржа</th>
            </tr>
            </thead>
            @foreach($cryptocurrencies as $cryptocurrency)
                <tr>
                    <th scrope="row"><img width="30" height="30" src="/res/coins/{{$cryptocurrency->icon_img_path}}">
                    </th>
                    <td>{{$cryptocurrency->short_name}}</td>
                    <td>{{$cryptocurrency->price * 0.95}} $</td>
                    <td><a href="{{route('crypto',$cryptocurrency->id)}}">
                            <button type="button" class="btn btn-success">Купить</button>
                        </a></td>
                </tr>
            @endforeach
            @else
                <h1 class="text-center text-danger"><i class="fa fa-search" aria-hidden="true"></i>
                    404
                </h1>
            @endif
            <tbody>
            </tbody>
        </table>
</div>
<div class="container">
</div>
</body>
</html>
