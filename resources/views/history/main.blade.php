<?php
// config
$link_limit = 7;
?>
    <!doctype html>
<html lang="{{app()->getLocale()}}">
@include('layoutAuth',['title'=>'История'])

<body>
@include('navbar')
<div class="container-fluid">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Тип</th>
            <th scope="col">Сумма</th>
            <th scope="col">Агент</th>
        </tr>
        </thead>
        <tbody>
        @foreach($history as $note)
            <tr>
                <th scope="row">{{$note->id}}</th>
                @if($note->type === "refill")
                    <td>Пополнение</td>
                @elseif($note->type === "buy")
                    <td>Покупка</td>
                @elseif($note->type === "bonus")
                    <td>Бонус</td>
                @endif
                @if($note->type === "buy")
                    <td><span class="text-danger">-{{$note->value}}</span>$</td>
                @elseif($note->type === "refill")
                    <td><span class="text-success">+{{$note->value}}</span>$</td>
                @elseif($note->type === "bonus")
                    <td><span class="text-success">+{{$note->value}}</span>$</td>
                @endif
                <td>{{$note->from_title}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item {{\App\Http\Helper\PaginatorHelper\PaginatorHelper::getFirstPage(
    $history->currentPage())
    ? 'disabled' : ''
    }}">
                <a class="page-link" href="{{$history->url($history->currentPage() - 1)}}" tabindex="-1"
                   aria-disabled="true">Предыдущая</a>
            </li>
            <li class="page-item
            {{\App\Http\Helper\PaginatorHelper\PaginatorHelper::getLastPage(
    $history->lastPage(),
    $history->currentPage())
    ? 'disabled' : ''
    }}">
                <a class="page-link" href="{{$history->url($history->currentPage() + 1)}}">Следующая</a>
            </li>
        </ul>
    </nav>
</div>
</body>
</html>
