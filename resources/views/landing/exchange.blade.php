<!doctype html>
<html lang="{{app()->getLocale()}}">
@include('layoutAuth',['title'=>'Биржа'])
<body>
<div class="container-sm" style="text-align: center; margin-top: 50px">

    @if($cryptocurrencies->first())
        <table class="table">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Название</th>
                <th scope="col">Курс ~$(-5%)</th>
                <th scope="col">Биржа</th>
            </tr>
            </thead>
            @foreach($cryptocurrencies as $cryptocurrency)
                <tr>
                    <th scrope="row"><img width="30" height="30" src="/res/coins/{{$cryptocurrency->icon_img_path}}">
                    </th>
                    <td>{{$cryptocurrency->short_name}}</td>
                    <td>{{$cryptocurrency->price}} $</td>
                    <td><a href="{{route('crypto',$cryptocurrency->id)}}">
                            <button type="button" class="btn btn-success">Купить</button>
                        </a></td>
                </tr>
            @endforeach
            @else
                <h1 class="text-center text-danger"><i class="fa fa-search" aria-hidden="true"></i>
                    404
                </h1>
            @endif
            <tbody>
            </tbody>
        </table>
</div>
</body>
</html>
