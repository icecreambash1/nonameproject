<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
<header class="header">
    <div class="header__body">
        <p>Trible Exchange</p>
        <a href="{{route('login')}}" class="header__login">Войти</a>
    </div>
</header>

<main class="main">
    <section>
        <h1 class="landing__title">
            Покупайте, торгуйте и храните криптовалоюту на Trible Exchange.
        </h1>

        <div class="landing__card">
            <p>
                Регистрируйтесь на платформе Trible Exchange! Мы предоставляем
                удобный сервис и бонус для новых пользователей: 5% скидку на все валюты, которая
                позволит вам начать торговать на бирже с намного меньшим риском.
            </p>
            <img src="{{asset('res/landing/go-cooperate.jpg')}}" alt="Скидка">
        </div>
    </section>

    <section>
        <h2 class="landing__title">
            У нас имеется удобный интерфейс
        </h2>

        <div class="landing__card">
            <img src="{{asset('res/landing/example.png')}}" alt="Интерфейс">
            <p>
                Наш сервис предоставляет удобный интерфейс
                с сортировкой по нескольким параметрам. Кроме того вся необходимая Вам информация уже выведена в
                компактном табличном виде.
            </p>
        </div>
    </section>

    <section>
        <h2 class="landing__title">
            С нами легко заработать. Попробуй уже сейчас.
        </h2>
        <div class="landing__card">
            <div class="landing-reg">
                <a href="{{route('exchange')}}" class="landing_reg-btn">
                    Попробуйте биржу
                </a>
            </div>
            <img src="{{asset('res/landing/go-reg.jpg')}}" alt="Регистрация">
        </div>
    </section>

</main>

<footer>
    <div class="footer__body">
        <span>© Trible Exchange</span>
    </div>

</footer>
</body>

<style>

    html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}
    article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}
    html{height:100%}
    body{line-height:1}
    ol,ul{list-style:none}
    blockquote,q{quotes:none}
    blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}
    table{border-collapse:collapse;border-spacing:0}
    a{
        text-decoration: none;
    }
    html{
        font-family: "Arial", cursive;
        font-size: 1.65vh;
        color: #000000;
    }
    body{
        background: #ffffff;
    }

    .header{
        font-family: "Comic Sans MS";
        background-color: #1F2229;
        position: fixed;
        width: 100%;
        color: #ffffff;
    }
    .header__body{
        display: flex;
        justify-content: space-between;
        padding: 12px 160px;
        max-width: 1600px;
        text-align: center;
        align-items: center;

    }

    .header__body p{
        margin-right: 18px;
        width: 48px;
        font-size: 2.3vmin;
    }

    .input{
        padding: 16px;
        background: #c5ced3;
        border-radius: 5px;
        border: 0;
    }
    .header__body input{
        margin-right: 1rem;
    }
    .header__login{
        margin-left: auto;
        padding: 12px 42px;
        font-size: 20px;
        line-height: 28px;
        border-radius: 12px;
        background-color: #c5ced3;
        color: #000;
    }


    .transaction{
        width: 255px;
        position: fixed;
        top: 90px;
        left: 45%;
    }

    .transaction__btn{
        padding: 2px 32px;
        background: #0D1017;
        border-radius: 5px;
    }


    .btn-buy{
        color: #005600;

    }

    .btn-sale{
        color: #8F0000;
    }


    .footer__body{
        margin-top: 32px;
        display: flex;
        justify-content: space-between;
        padding: 18px 160px;

        text-align: center;
        background-color: #1F2229;

    }

    .footer__body span, .footer__body a{
        font-size: 20px;
        color: white;
    }
    html{
        font-size: 1.64vh;
    }
    h1{
        font-weight: bold;
    }

    .main{
        max-width: 1600px;
        text-align: center;
        padding: 52px 160px 0;
    }

    section{
        margin-top: 48px;

    }

    .landing__title{
        font-size: 48px;
        max-width: 1200px;
        margin: 0 auto;
    }

    .landing__card{
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 40px;
        margin-top: 32px;
    }
    .landing__card p{
        text-align: left;
        max-width: 690px;
        margin: 1rem;
        font-size: 2.3vw;
    }
    .landing__card img{
        width: 40vw;
        border-radius: 20px;
        margin: 1rem;
    }
    .landing-reg{
        display: flex;
        flex-direction: column;
        width: 40vw;
    }
    .landing__input{
        width: 40vw;
        font-size: 32px;
        box-sizing: border-box;
    }
    .landing_reg-btn{
        margin-top: 32px;
        padding: 16px;
        background-color: green;
        color: #000;
        font-size: 2vw;
        border-radius: 10px;
        overflow: hidden;
    }
    @media (max-width: 992px) {
        .header__body input{
            max-width: 40%;
        }
        .header__body p{
            font-size: 3vw;
        }
        .header__login{
            padding: 1rem;
        }
        .landing__card{
            flex-direction: column;
        }

        Дмитрий Липатников, [29.05.2022 11:55]
        .landing__title{
            font-size: 2.5rem;
        }
        .landing__card p{
            font-size: 1.5rem;
            line-height: normal;
        }
        .landing__card img{
            margin-top: 2rem;
            width: 60%;
        }
        .landing-reg{
            max-width: 100%;
        }
        .landing__input, .landing_reg-btn{
            width: 100%;
            font-size: 3vw;
            font-family: "Comic Sans MS";
        }
        .header__body, .footer__body{
            padding: 18px 18px;
        }
        .main{
            padding: 52px 18px;
        }
    }
</style>

</html>
