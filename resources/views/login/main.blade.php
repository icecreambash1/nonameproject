<!doctype html>
<html lang="{{app()->getLocale()}}">
@include('layoutAuth',['title'=>'Логин'])
<body>
<div class="container">
    <section class="vh-100">
        <div class="container py-5 h-100">
            <div class="row d-flex align-items-center justify-content-center h-100">
                <div class="col-md-8 col-lg-7 col-xl-6">
                    <img src="{{asset('res/auth/img_logo.png')}}"
                         class="img-fluid" alt="Phone image">
                </div>
                <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                    <form action="{{route('login')}}" method="post">
                        @csrf
                        <!-- Email input -->
                        <div class="form-outline mb-4">
                            <input name="login" type="text" id="form1Example13" class="form-control form-control-lg" />
                            @if($errors->has('login'))
                            @error('login')
                                <label class="form-label text-danger" for="form1Example13">{{$message}}</label>
                            @enderror
                            @else
                                <label class="form-label" for="form1Example13">Логин</label>
                            @endif
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-4">
                            <input name="password" type="password" id="form1Example23" class="form-control form-control-lg" />
                            @if($errors->has('password'))
                                @error('password')
                                <label class="form-label text-danger" for="form1Example13">{{$message}}</label>
                                @enderror
                            @else
                                <label class="form-label" for="form1Example13">Пароль</label>
                            @endif
                        </div>

                        <div class="d-flex justify-content-around align-items-center mb-4">
                            <a href="{{route('register')}}">Вы хотите зарегистрироваться?</a>
                        </div>

                        <!-- Submit button -->
                        <div class="d-flex justify-content-around align-items-center mb-4">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Войти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>
