<nav class="navbar navbar-expand-lg navbar-light bg-light rounded" aria-label="Eleventh navbar example">
    <div class="container-fluid">
        <a class="navbar-brand">TribalExchange</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample09"
                aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample09">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{route('lk')}}">Дашборд</a>
                </li>
            </ul>
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown09" data-bs-toggle="dropdown"
                       aria-expanded="false">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        {{\App\Http\Helper\AuthHelper\AuthHelper::user()->login}}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown09">
                        <li><a class="dropdown-item" href="{{route('history')}}"><i class="fa fa-list-alt" aria-hidden="true"></i>
                                 История</a></li>
                        <li><a class="dropdown-item" href="{{route('balance')}}"><i class="fa fa-credit-card"
                                                                                    aria-hidden="true"></i>
                                {{\App\Http\Helper\AuthHelper\AuthHelper::user()->cash}} $</a></li>
                        <li><a class="dropdown-item text-danger" href="{{route('logout')}}">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
