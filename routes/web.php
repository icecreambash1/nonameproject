<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', [\App\Http\Controllers\LandingController::class, 'index'])->name('landing')->middleware('user-was-auth');
Route::get('/exchange', [\App\Http\Controllers\LandingController::class,'exchange'])->name('exchange');
Route::group(['prefix' => '/auth', 'middleware' => 'user-was-auth'], function () {
    Route::get('/register', [\App\Http\Controllers\Auth\RegisterController::class, 'index'])->name('register');
    Route::post('/register', [\App\Http\Controllers\Auth\RegisterController::class, 'store'])->name('register');

    Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'index'])->name('login');
    Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'store'])->name('login');


});

Route::group(['prefix' => '/lk', 'middleware' => 'user-auth'], function () {
    Route::get('/', [\App\Http\Controllers\LK\Dashboard\DashboardController::class, 'index'])->name('lk');
    Route::post('/', [\App\Http\Controllers\LK\Dashboard\DashboardController::class, 'searchCrypto'])->name('searchCrypto');
    Route::get('/history', [\App\Http\Controllers\LK\Dashboard\HistoryController::class, 'index'])->name('history');
    Route::get('/balance', [\App\Http\Controllers\LK\Dashboard\BalanceController::class, 'index'])->name('balance');
    Route::post('/balance', [\App\Http\Controllers\LK\Dashboard\BalanceController::class, 'store'])->name('balance');
    Route::get('/crypto/{id}', [\App\Http\Controllers\LK\Crypto\CryptoController::class, 'index'])->name('crypto');
    Route::post('/crypto/{id}', [\App\Http\Controllers\LK\Crypto\CryptoController::class, 'store'])->name('crypto');
});

Route::group(['prefix' => '/user'], function () {
    Route::group([], function () {

    });
    Route::get('/logout', [\App\Http\Controllers\Auth\LogoutController::class, 'index'])->name('logout');

});



